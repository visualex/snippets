                function DoFullScreen() {
                        var elem = document.getElementById("main_image");

                        image = $(elem).children('img');

                        dimensions = {width: image.width, height: image.height};

                        if (elem.requestFullscreen) {
                            elem.requestFullscreen();
                        } else if (elem.mozRequestFullScreen) {
                            elem.mozRequestFullScreen();
                        } else if (elem.webkitRequestFullscreen) {
                            elem.webkitRequestFullscreen();
                        }

                        image.width(dimensions.width * 2);
                        image.height(dimensions.height * 2);

                    }

<?

/* ----------------------------------app/app_controller.php---------------------- */




//ADD AN EMPTY FIELD TO SCAFFOLDED SELECTS :: /app/cake/libs/view/scaffolds/edit.ctp
//this must be added on top of the form creation
$scaffoldFields = array_flip($scaffoldFields);
foreach ($scaffoldFields as &$v) {
    $v = array('empty' => '');
}

//THIS WILL PROBBABLY FORM PART OF CAKEPHP 3 (HOPE SO)
//turn arrays into objects_ to use arrays like this
// $product->Product->id
class ViewParser
{

    public function parse(array $arr, stdClass $parent = null)
    {
        if ($parent === null) {
            $parent = $this;
        }
        foreach ($arr as $key => $val) {
            if (is_array($val)) {
                $parent->$key = $this->parse($val, new stdClass);
            } else {
                $parent->$key = $val;
            }
        }
        return $parent;
    }

}

//
//$view = array(
//    'Product' =>
//        array(
//            'a' => 1,
//            'b' => 2,
//            'c' => 3
//    ),
//    'User' =>
//        array(
//            'name' => 'a',
//            'email' => 'a@q.c',
//        )
//);
//
//$vp = new ViewParser();
//echo var_dump($vp->parse($view));
//$View = $vp->parse($view);
//
//foreach($View->User as $key => $value){
//    echo ($value);
//}
//A WAY TO EXTEND A CONTROLLER
App::import('Controller', 'Users');

class UsersExtendedController extends UsersController
{

    var $name = 'Users';
    var $helpers = array('Xml');
    var $components = array('Email');
    var $uses = array('User', 'UserGroup');

    function admin_user_panel($user_id = '')
    {
        /* ... */
    }

}

